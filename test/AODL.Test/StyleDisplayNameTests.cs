using System.IO;
using System.Linq;
using AODL.Document.Content.Text;
using AODL.Document.Styles;
using AODL.Document.TextDocuments;
using WillowMedia.Common.IO;
using Xunit;

namespace AODL.Test;

public class StyleDisplayNameTests
{
    protected FileInfo GetTemplate()
    {
        var templateFile = PathHelpers.FileInfo("./Resources/examen-mc.odt");
        Assert.True(templateFile?.Exists);
        return templateFile;
    }
    
    [Fact]
    public void FindDisplayStyleFromCommonStylesTest()
    {
        var templateFile = GetTemplate();
        var outputFile = new FileInfo(PathHelpers.GetTempFileName("odt"));

        using (var document = new TextDocument())
        {
            document.Load(templateFile.FullName);

            var commonstyle = document.CommonStyles.FirstOrDefault(s =>
                s.Node.Attributes["style:display-name"]?.Value == "Text body MC Question index");

            Assert.NotNull(commonstyle);
        }
    }
    
    [Fact]
    public void FindDisplayStyleFromPropertiesTest()
    {
        var templateFile = GetTemplate();
        var outputFile = new FileInfo(PathHelpers.GetTempFileName("odt"));

        using (var document = new TextDocument())
        {
            document.Load(templateFile.FullName);

            var style = document.CommonStyles.FirstOrDefault(
                s => s is ParagraphStyle style && style.StyleDisplayName == "Text body MC Question index");

            Assert.NotNull(style);
        }
    }
}