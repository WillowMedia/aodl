using System.Diagnostics;
using System.Runtime.InteropServices;
using AODL.Document;
using AODL.Document.Content;
using AODL.Document.Content.Tables;
using AODL.Document.Content.Text;
using AODL.Document.Styles;
using AODL.Document.Styles.Properties;
using AODL.Document.TextDocuments;
using WillowMedia.Common.IO;
using Xunit;

namespace AODL.Test;

public class CreateDocument
{
    [Fact]
    public void CreateTest()
    {
        string fileToPrint = PathHelpers.GetTempFileName("odt");

        //Create a new text document
        using (var document = new TextDocument())
        {
            document.New();
            //Create a table for a text document using the TableBuilder
            var table = TableBuilder.CreateTextDocumentTable(
                document,
                "table1",
                "table1",
                3,
                3,
                16.99,
                false,
                false);
            //Create a standard paragraph
            var paragraph = ParagraphBuilder.CreateStandardTextParagraph(document);
            //Add some simple text
            paragraph.TextContent.Add(new SimpleText(document, "Some cell text"));
            //Insert paragraph into the first cell
            table.Rows[0].Cells[0].Content.Add(paragraph);
            //Add table to the document
            document.Content.Add(table);
            //Save the document
            document.SaveTo(fileToPrint);
        }
        
        if (Debugger.IsAttached && RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            WillowMedia.Common.IO.Cli.Process.Execute("open", fileToPrint);
    }

    [Fact]
    public void Sample()
    {
        // http://wiki.openoffice.org/wiki/AODL_example_17
        string fileToPrint = PathHelpers.GetTempFileName("odt");

        //Create a new text document
        using (var document = new TextDocument())
        {
            document.New();
            //Create a standard paragraph using the ParagraphBuilder
            var paragraph = ParagraphBuilder.CreateStandardTextParagraph(document);
            //Add some simple text
            paragraph.TextContent.Add(new SimpleText(document, "Some simple text!"));
            //Add the paragraph to the document
            document.Content.Add(paragraph);
            //Save empty
            document.SaveTo(fileToPrint);
        }

        if (Debugger.IsAttached && RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            WillowMedia.Common.IO.Cli.Process.Execute("open", fileToPrint);
    }

    [Fact]
    public void Sample2()
    {
        // http://aodl.sourceforge.net/about.html
        string fileToPrint = PathHelpers.GetTempFileName("odt");

        using (var document = new TextDocument())
        {
            document.New();

            var paragraph = new Paragraph(document, "P1");
            (paragraph.Style as ParagraphStyle).ParagraphProperties.Alignment = TextAlignments.end.ToString();

            paragraph.TextContent.Add(new SimpleText(document, "Hallo i'm simple text!"));

            var ft = new FormatedText(document, "T1", " And i'm formated text!");
            (ft.Style as TextStyle).TextProperties.Bold = "bold";
            (ft.Style as TextStyle).TextProperties.Italic = "italic";
            (ft.Style as TextStyle).TextProperties.Underline = LineStyles.dotted.ToString();
            paragraph.TextContent.Add(ft);

            document.Content.Add(paragraph);

            document.SaveTo(fileToPrint);
        }

        if (Debugger.IsAttached && RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            WillowMedia.Common.IO.Cli.Process.Execute("open", fileToPrint);
    }
}