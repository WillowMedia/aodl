using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;
using AODL.Document.Content.Draw;
using AODL.Document.Content.Text;
using AODL.Document.Import;
using AODL.Document.Styles;
using AODL.Document.TextDocuments;
using AODL.Extensions;
using WillowMedia.Common.IO;
using Xunit;

namespace AODL.Test;

public class ImageTests
{
    [Fact]
    public void ImageTestFile()
    {
        var testFile = PathHelpers.Normalize("./Resources/image-test.odt");
        Assert.True(File.Exists(testFile));

        using (var document = new TextDocument())
        {
            document.Load(testFile);
            
            // Find image
            var frames = document.Content.FindContent(typeof(Frame));
            Assert.NotEmpty(frames);

            var frame = frames.FirstOrDefault() as Frame;
            // var properties = frame.FrameStyle.PropertyCollection;
            var frameStyle = frame.FrameStyle;
            Assert.NotNull(frameStyle);

            var properties = frameStyle.GraphicProperties;
            Assert.NotNull(properties.Wrap);
            Assert.Equal("none", properties.Wrap, StringComparer.InvariantCultureIgnoreCase);
        }
    }
    
    [Fact]
    public void CreateDocumentWithImage()
    {
        var imageFile = PathHelpers.Normalize("./Resources/test.jpg");
        var fileToPrint = PathHelpers.GetTempFileName("odt");
        var elementCount = 0;
        
        Assert.True(File.Exists(imageFile));
        
        //Create a new text document
        using (var document = new TextDocument())
        {
            document.New();

            var frame = FrameBuilder.BuildStandardGraphicFrame(
                document, $"frame_{elementCount++}", $"graphic_{elementCount++}", imageFile);
            
            frame.SvgWidth = "4cm";
            // frame.SvgHeight = "auto";


            // var rects = new DrawAreaRectangle[1];
            // rects[0] = new DrawAreaRectangle(document, "4cm", "4cm", "2cm", "2cm");

            // Create and add an image map, referencing the area rectangles
            // var map = new ImageMap(document, rects);

            //frame.Content.Add(map);

            // Add the frame to the text document
            document.Content.Add(frame);

            // Save file
            document.SaveTo(fileToPrint);
        }
        
        if (Debugger.IsAttached && RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            WillowMedia.Common.IO.Cli.Process.Execute("open", fileToPrint);
    }
    
    /// <summary>
    /// Documents with WMF images can't be opened due to ImageSharp exception
    /// </summary>
    [Fact]
    public void DocumentWithWmfFile()
    {
        var testFile = PathHelpers.Normalize("./Resources/examen-general-wmf-logo.ott");
        Assert.True(File.Exists(testFile));

        using (var document = new TextDocument())
        {
            // Assert.Throws<ImporterException>(() => document.Load(testFile));
            document.Load(testFile);
        }
    }
    
    [Fact]
    public void DocumentWithSvgFile()
    {
        var testFile = PathHelpers.Normalize("./Resources/examen-general-svg-logo.ott.odt");
        Assert.True(File.Exists(testFile));

        using (var document = new TextDocument())
        {
            document.Load(testFile);

            var tempFile = PathHelpers.GetTempFileInfo("odt");
            document.SaveTo(tempFile.FullName);
            
            if (Debugger.IsAttached && RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                WillowMedia.Common.IO.Cli.Process.Execute("open", tempFile.FullName);
        }
    }
}