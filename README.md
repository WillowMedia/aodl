# Aodl

Attempt to actively develop a (or the) AODL code for generation ODF files with Dot Net.

## Roadmap

- First migrate to Dot Net core
- Migrate some of the tests
- Refactor and improve the code
- Remove SharpZipLib requirement

## Info

Result files can be tested with [ODF Validator](https://odfvalidator.org)

## source

Fork of [aodl2-code](https://sourceforge.net/p/aodl2/code/HEAD/tree/)
See also ```svn checkout https://svn.code.sf.net/p/aodl2/code/trunk aodl2-code```