using System.IO;
using System.Linq;
using System.Reflection;

namespace WillowMedia.Aodl
{
    public static class AssemblyExtensions
    {
        public static Stream GetManifestResourceStreamNameContains(this Assembly assembly, string contains)
        {
            var name = assembly.GetManifestResourceNames()
                .FirstOrDefault(r => r.Contains(contains));

            if (string.IsNullOrEmpty(name))
                return null;

            return assembly.GetManifestResourceStream(name);
        }
    }
}