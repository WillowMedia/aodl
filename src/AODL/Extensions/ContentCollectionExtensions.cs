using System;
using System.Collections.Generic;
using AODL.Document.Content;

namespace AODL.Extensions;

public static class ContentCollectionExtensions {
    public static List<IContent> FindContent(this ContentCollection collection, Type contentType)
    {
        var store = new List<IContent>();

        collection.FindContent(store, contentType);
        
        return store;
    }

    private static void FindContent(this ContentCollection collection, List<IContent> store, Type contentType)
    {
        foreach (var content in collection)
        {
            var type = content.GetType();

            if (type == contentType)
                store.Add(content);

            if (content is IContentContainer contentContainer)
                contentContainer.Content.FindContent(store, contentType);
            else if (content is ContentCollection childContentContainer) 
                childContentContainer.FindContent(store, contentType);
        }
    }
}